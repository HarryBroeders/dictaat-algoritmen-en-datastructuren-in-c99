# Dictaat Algoritmen en Datastructuren in C99 #

In dit repository is het dictaat Algoritmen en Datastructuren in C99 opgenomen. Dit is een vertaling naar C van het C++-dictaat [Algoritmen en Datastructuren in C++](https://bitbucket.org/HarryBroeders/dictaat-algoritmen-en-datastructuren-in-c). Slechts paragraaf 7.1 van dit C++-dictaat is op dit moment vertaald naar C.

Je kunt de gecompileerde versies (in pdf) vinden in de [Downloads](https://bitbucket.org/HarryBroeders/dictaat-algoritmen-en-datastructuren-in-c99/downloads) van dit repository.

Op- en aanmerkingen zijn altijd welkom. Maak een [issue](https://bitbucket.org/HarryBroeders/dictaat-algoritmen-en-datastructuren-in-c99/issues?status=new&status=open) aan of stuur een mail naar [Harry Broeders](mailto:harry@hc11.demon.nl).