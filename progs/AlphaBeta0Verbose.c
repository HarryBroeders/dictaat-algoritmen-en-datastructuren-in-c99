#include <stdio.h>

int value(int pos);
int chooseComputerMove(int pos, int alpha, int beta);
int chooseHumanMove(int pos, int alpha, int beta);

const int UNDECIDED = -1;

int value(int pos)
{
    static const int value[16] = { 4, 5, 3, 2, 6, 7, 8, 9, 1, 10, 2, 11, 12, 13, 14, 14 };
    if (pos >= 15 && pos < 31)
    {
        return value[pos - 15]; // return known value
    }
    return UNDECIDED;
}

int chooseComputerMove(int pos, int alpha, int beta)
{
    int bestValue = value(pos);
    if (bestValue == UNDECIDED)
    {
        bestValue = alpha;
        int i;
        for (i = 1; bestValue < beta && i < 3; i++)
        {
            int value = chooseHumanMove(2 * pos + i, alpha, beta);
            if (value > bestValue)
            {
                bestValue = value;
                alpha = bestValue;
            }
        }
        if (bestValue >= beta && i < 3)
        {
            printf("snoei node %d\n", 2 * pos + i);
        }
    }
    return bestValue;
}

int chooseHumanMove(int pos, int alpha, int beta)
{
    int bestValue = value(pos);
    if (bestValue == UNDECIDED)
    {
        bestValue = beta;
        int i;
        for (i = 1; bestValue > alpha && i < 3; i++)
        {
            int value = chooseComputerMove(2 * pos + i, alpha, beta);
            if (value < bestValue)
            {
                bestValue = value;
                beta = bestValue;
            }
        }
        if (bestValue <= alpha  && i < 3)
        {
            printf("snoei node %d\n", 2 * pos + i);
        }
    }
    return bestValue;
}

int main(void)
{
    int value = chooseComputerMove(0, 0, 15);
    printf("Minimaal te behalen Maximale waarde = %d\n", value);
    return 0;
}
