#include <stdio.h>

typedef struct
{
    int val;
    int nPos;
} ValueNextPos;

int positionValue(int pos);
ValueNextPos valueMoveComputer(int pos, int alpha, int beta);
ValueNextPos valueMoveHuman(int pos, int alpha, int beta);

const int UNDECIDED = -1;

int positionValue(int pos)
{
    static const int value[16] = { 4, 5, 3, 2, 6, 7, 8, 9, 1, 10, 2, 11, 12, 13, 14, 14 };
    return pos >= 15 && pos <= 31 ? value[pos - 15] : UNDECIDED;
}

ValueNextPos valueMoveComputer(int pos, int alpha, int beta)
{
    int value = positionValue(pos);
    if (value != UNDECIDED)
    {
        return (ValueNextPos){value, pos};
    }
    int posL = 2 * pos + 1;
    int posR = 2 * pos + 2;
    ValueNextPos moveL = valueMoveHuman(posL, alpha, beta);
    if (moveL.val > alpha)
    {
        alpha = moveL.val;
        if (alpha >= beta)
        {
            return (ValueNextPos){alpha, posL};
        }
    }
    ValueNextPos moveR = valueMoveHuman(posR, alpha, beta);
    return moveR.val > alpha ? (ValueNextPos){moveR.val, posR} : (ValueNextPos){alpha, posL};
}

ValueNextPos valueMoveHuman(int pos, int alpha, int beta)
{
    int value = positionValue(pos);
    if (value != UNDECIDED)
    {
        return (ValueNextPos){value, pos};
    }
    int posL = 2 * pos + 1;
    int posR = 2 * pos + 2;
    ValueNextPos moveL = valueMoveComputer(posL, alpha, beta);
    if (moveL.val < beta)
    {
        beta = moveL.val;
        if (beta <= alpha)
        {
            return (ValueNextPos){beta, posL};
        }
    }
    ValueNextPos moveR = valueMoveComputer(posR, alpha, beta);
    return moveR.val < beta ? (ValueNextPos){moveR.val, posR} : (ValueNextPos){beta, posL};
}

int main(void)
{
    int pos = 0;
    while (pos < 15)
    {
        ValueNextPos res = valueMoveComputer(pos, 0, 15);
        printf("Minimaal te behalen Maximale waarde = %d\n", res.val);
        pos = res.nPos;
        printf("Computer kiest positie: %d\n", pos);
        int posL = 2 * pos + 1;
        int posR = 2 * pos + 2;
        if (pos < 15)
        {
            printf("Je kunt kiezen voor positie %d of positie %d\n", posL, posR);
            printf("Pssst, %d is de beste keuze.\n", valueMoveHuman(pos, 0, 15).nPos);
            do
            {
                printf("Maak je keuze: ");
                fflush(stdin);
            }
            while (scanf("%d", &pos) != 1 && pos != posL && pos != posR);
        }
    }
    printf("Behaalde waarde = %d\n", positionValue(pos));
    return 0;
}
