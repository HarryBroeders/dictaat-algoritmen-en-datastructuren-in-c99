#include <stdio.h>

int value(int pos);
int chooseComputerMove(int pos, int *bestNextPos, int alpha, int beta);
int chooseHumanMove(int pos, int *bestNextPos, int alpha, int beta);

const int UNDECIDED = -1;

int value(int pos)
{
    static const int value[16] = { 4, 5, 3, 2, 6, 7, 8, 9, 1, 10, 2, 11, 12, 13, 14, 14 };
    if (pos >= 15 && pos < 31)
    {
        return value[pos - 15]; // return known value
    }
    return UNDECIDED;
}

int chooseComputerMove(int pos, int *bestNextPos, int alpha, int beta)
{
    int bestValue = value(pos);
    if (bestValue == UNDECIDED)
    {
        for (int i = 1; alpha < beta && i < 3; i++)
        {
            int dummyPos;
            int value = chooseHumanMove(2 * pos + i, &dummyPos, alpha, beta);
            if (value > alpha)
            {
                alpha = value;
                *bestNextPos = 2 * pos + i;
            }
        }
        bestValue = alpha;
    }
    return bestValue;
}

int chooseHumanMove(int pos, int *bestNextPos, int alpha, int beta)
{
    int bestValue = value(pos);
    if (bestValue == UNDECIDED)
    {
        for (int i = 1; alpha < beta && i < 3; i++)
        {
            int dummyPos;
            int value = chooseComputerMove(2 * pos + i, &dummyPos, alpha, beta);
            if (value < beta)
            {
                beta = value;
                *bestNextPos = 2 * pos + i;
            }
        }
        bestValue = beta;
    }
    return bestValue;
}

int main(void)
{
    int pos = 0, bestNextPos, bestValue;
    while (pos < 15)
    {
        bestValue = chooseComputerMove(pos, &bestNextPos, 0, 15);
        printf("Minimaal te behalen Maximale waarde = %d\n", bestValue);
        pos = bestNextPos;
        printf("Computer kiest positie: %d\n", pos);
        int posL = 2 * pos + 1;
        int posR = 2 * pos + 2;
        if (pos < 15)
        {
            printf("Je kunt kiezen voor positie %d of positie %d\n", posL, posR);
            chooseHumanMove(pos, &bestNextPos, 0, 15);
            printf("Pssst, %d is de beste keuze.\n", bestNextPos);
            do
            {
                printf("Maak je keuze: ");
                fflush(stdin);
            }
            while (scanf("%d", &pos) != 1 && pos != posL && pos != posR);
        }
    }
    printf("Behaalde waarde = %d\n", value(pos));
    return 0;
}
