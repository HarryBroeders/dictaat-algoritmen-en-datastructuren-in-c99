// test program for stopwatch
// run compiler with and without optimizer
#include <stdio.h>
#include <stdbool.h>

#define WIN32
#ifdef WIN32
    #include <windows.h>
#else
    #include <time.h>
#endif

typedef struct
{
    bool running;
#ifdef WIN32
    double frequency;
    long long start_time;
    long long total_time;
#else
    clock_t start_time;
    clock_t total_time;
#endif
} StopWatchData;

void initStopWatch(StopWatchData *pStopWatchData);
void startStopWatch(StopWatchData *pStopWatchData);
void stopStopWatch(StopWatchData *pStopWatchData);
#ifdef WIN32
long long getExecutionTime(const StopWatchData *pStopWatchData);
#else
clock_t getExecutionTime(const StopWatchData *pStopWatchData);
#endif
void printStopWatchError();
double readStopWatchTime(const StopWatchData *pStopWatchData);
void printStopWatchTime(const StopWatchData *pStopWatchData);

void initStopWatch(StopWatchData *pStopWatchData)
{
    pStopWatchData->running = false;
    pStopWatchData->start_time = 0;
#ifdef WIN32
    LARGE_INTEGER performanceFrequency;
    if (QueryPerformanceFrequency(&performanceFrequency) == 0)
    {
        printStopWatchError();
    }
    pStopWatchData->frequency = (double)performanceFrequency.QuadPart;
#endif
}

void startStopWatch(StopWatchData *pStopWatchData)
{
    if (!pStopWatchData->running)
    {
        pStopWatchData->running = true;
        pStopWatchData->start_time = getExecutionTime(pStopWatchData);
    }
}

void stopStopWatch(StopWatchData *pStopWatchData)
{
    if (pStopWatchData->running)
    {
        pStopWatchData->running = false;
        pStopWatchData->total_time = getExecutionTime(pStopWatchData) - pStopWatchData->start_time;
    }
}

#ifdef WIN32
long long getExecutionTime(const StopWatchData *pStopWatchData)
{
    LARGE_INTEGER performanceCount;
    if (QueryPerformanceCounter(&performanceCount) == 0)
    {
        printStopWatchError();
        return 0;
    }
    return performanceCount.QuadPart;
}
#else
clock_t getExecutionTime(const StopWatchData *pStopWatchData)
{
    clock_t t = clock();
    if (t == -1)
    {
        printStopWatchError();
        return 0;
    }
    return clock();
}
#endif

void printStopWatchError()
{
#ifdef WIN32
    LPTSTR lpMsgBuf = 0;
    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        GetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        lpMsgBuf,
        0,
        NULL
    );
    fprintf(stderr, "%s", lpMsgBuf);
    LocalFree(lpMsgBuf);
#else
    fprintf(stderr, "No timer available\n");
#endif
}

double readStopWatchTime(const StopWatchData *pStopWatchData)
{
#ifdef WIN32
    long long res = pStopWatchData->running ? getExecutionTime(pStopWatchData) - pStopWatchData->start_time : pStopWatchData->total_time;
    return res / pStopWatchData->frequency;
#else
    clock_t res = pStopWatchData->running ? getExecutionTime(pStopWatchData) - pStopWatchData->start_time : pStopWatchData->total_time;
    return (double)res / CLK_TCK;
#endif
}

void printStopWatchTime(const StopWatchData *pStopWatchData)
{
    printf("%f sec", readStopWatchTime(pStopWatchData));
}

int main(void)
{
    StopWatchData stopWatch1;
    initStopWatch(&stopWatch1);
    startStopWatch(&stopWatch1);
    int dummy;
    for (int i = 0; i < 100000000; ++i) {
        dummy = dummy * i;
    }
    stopStopWatch(&stopWatch1);
    printStopWatchTime(&stopWatch1);
    return 0;
}
