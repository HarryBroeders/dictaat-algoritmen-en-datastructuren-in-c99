#include <stdio.h>
#include <stdbool.h>

// this program is written to analyze a problem with the TicTacAB program
// the algorithm does not choose a value when it is in a losing situation

// define SOLUTION 0 to show the problem

// define SOLUTION 1 to show the first solution
// in this solution bestRow and bestColumn are initialized with the first valid move

// define SOLUTION 2 to show the second solution
// in this solution the bestValue is overridden if a value equal to bestValue is found

#define SOLUTION 1

// define ANALYSE to print the number of moves considered and time used
#define ANALYSE
// define WIN32 when you use a Windows OS
#define WIN32

#ifdef ANALYSE
#ifdef WIN32
#include <windows.h>
#else
#include <time.h>
#endif
typedef struct
{
    bool running;
#ifdef WIN32
    double frequency;
    long long start_time;
    long long total_time;
#else
    clock_t start_time;
    clock_t total_time;
#endif
} StopWatchData;
void initStopWatch(StopWatchData *pStopWatchData);
void startStopWatch(StopWatchData *pStopWatchData);
void stopStopWatch(StopWatchData *pStopWatchData);
double readStopWatchTime(const StopWatchData *pStopWatchData);
void printStopWatchTime(const StopWatchData *pStopWatchData);
int movesConsidered;
#endif

typedef enum {EMPTY, HUMAN, COMPUTER} Side;
typedef enum {HUMAN_WINS = -1, DRAW, COMPUTER_WINS, UNDECIDED} Value;

typedef Side Board[3][3];

void makeEmpty(Board board);
void print(Board board);
bool playMove(Board board, int row, int column, Side side);
bool isFull(Board board);
bool isSideTheWinner(Board board, Side side);

Value getValue(Board board);
bool isUndecided(Board board);
Value chooseComputerMove(Board board, int *bestRow, int *bestColumn, Value alpha, Value beta);
Value chooseHumanMove(Board board, int *bestRow, int *bestColumn, Value alpha, Value beta);
#if SOLUTION == 1
void findFirstValidMove(Board board, int *validRow, int *validColumn);
#endif
void doComputerMove(Board board);
void play(Board board, bool computerGoesFirst);
void lose(Board board);

void makeEmpty(Board board)
{
    for (int row = 0; row < 3; row++)
    {
        for (int column = 0; column < 3; column++)
        {
            {
                board[row][column] = EMPTY;
            }
        }
    }
}

void print(Board board)
{
    printf("---\n");
    for (int row = 0; row < 3; row++)
    {
        for (int column = 0; column < 3; column++)
            if (board[row][column] == COMPUTER)
            {
                printf("o");
            }
            else
            {
                if (board[row][column] == HUMAN)
                {
                    printf("x");
                }
                else
                {
                    printf(" ");
                }
            }
        printf("\n");
    }
    printf("---\n");
}

bool playMove(Board board, int row, int column, Side side)
{
    if (row < 0 || row >= 3 || column < 0 || column >= 3 || board[row][column] != EMPTY)
    {
        return false;
    }
    board[row][column] = side;
    return true;
}

bool isFull(Board board)
{
    for (int row = 0; row < 3; row++)
    {
        for (int column = 0; column < 3; column++)
        {
            if (board[row][column] == EMPTY)
            {
                return false;
            }
        }
    }
    return true;
}

bool isSideTheWinner(Board board, Side side)
{
    for (int i = 0; i < 3; i++)
    {
        if ((board[i][0] == side && board[i][1] == side && board[i][2] == side) ||
                (board[0][i] == side && board[1][i] == side && board[2][i] == side))
        {
            return true;
        }
    }
    return (board[0][0] == side && board[1][1] == side && board[2][2] == side) ||
           (board[0][2] == side && board[1][1] == side && board[2][0] == side);
}

Value getValue(Board board)
{
    return isSideTheWinner(board, COMPUTER) ? COMPUTER_WINS : isSideTheWinner(board, HUMAN) ? HUMAN_WINS : isFull(board) ? DRAW : UNDECIDED;
}

Value chooseComputerMove(Board board, int *bestRow, int *bestColumn, Value alpha, Value beta)
{
#ifdef ANALYSE
    ++movesConsidered;
#endif
    Value bestValue = getValue(board);
    if (bestValue == UNDECIDED)
    {
        for (int row = 0; alpha < beta && row < 3; row++)
        {
            for (int column = 0; alpha < beta && column < 3; column++)
            {
                if (board[row][column] == EMPTY)
                {
                    board[row][column] = COMPUTER;
                    int dummyRow, dummyColumn;
                    Value value = chooseHumanMove(board, &dummyRow, &dummyColumn, alpha, beta);
                    board[row][column] = EMPTY;
#if SOLUTION == 2
                    if (value >= alpha)
#else
                    if (value > alpha)
#endif
                    {
                        alpha = value;
                        *bestRow = row;
                        *bestColumn = column;
                    }
                }
            }
        }
        bestValue = alpha;
    }
    return bestValue;
}

Value chooseHumanMove(Board board, int *bestRow, int *bestColumn, Value alpha, Value beta)
{
#ifdef ANALYSE
    ++movesConsidered;
#endif
    Value bestValue = getValue(board);
    if (bestValue == UNDECIDED)
    {
        for (int row = 0; alpha < beta && row < 3; row++)
        {
            for (int column = 0; alpha < beta && column < 3; column++)
            {
                if (board[row][column] == EMPTY)
                {
                    board[row][column] = HUMAN;
                    int dummyRow, dummyColumn;
                    Value value = chooseComputerMove(board, &dummyRow, &dummyColumn, alpha, beta);
                    board[row][column] = EMPTY;
#if SOLUTION == 2
                    if (value <= beta)
#else
                    if (value < beta)
#endif
                    {
                        beta = value;
                        *bestRow = row;
                        *bestColumn = column;
                    }
                }
            }
        }
        bestValue = beta;
    }
    return bestValue;
}

#if SOLUTION == 1
void findFirstValidMove(Board board, int *validRow, int *validColumn)
{
    for (int row = 0; row < 3; ++row)
    {
        for (int column = 0; column < 3; ++column)
        {
            if (board[row][column] == EMPTY)
            {
                *validRow = row;
                *validColumn = column;
                return;
            }
        }
    }
}
#endif

void doComputerMove(Board board)
{
    int bestRow, bestColumn;
#ifdef ANALYSE
    movesConsidered = 0;
    StopWatchData stopWatch1;
    initStopWatch(&stopWatch1);
    startStopWatch(&stopWatch1);
#endif
#if SOLUTION == 1
    // choose the first valid move
    findFirstValidMove(board, &bestRow, &bestColumn);
    // search for a better move
#endif
    chooseComputerMove(board, &bestRow, &bestColumn, HUMAN_WINS, COMPUTER_WINS);
#ifdef ANALYSE
    stopStopWatch(&stopWatch1);
    printf("Calculation time: %f\n", readStopWatchTime(&stopWatch1));
    printf("Moves considered: %d\n", movesConsidered);
#endif
    printf("Computer plays: ROW = %d COLUMN = %d\n", bestRow, bestColumn);
    playMove(board, bestRow, bestColumn, COMPUTER);
}

bool isUndecided(Board board)
{
    return getValue(board) == UNDECIDED;
}

void play(Board board, bool computerGoesFirst)
{
    if (computerGoesFirst)
    {
        doComputerMove(board);
    }
    printf("\n");
    do
    {
        int row, column;
        do
        {
            print(board);
            printf("Enter row and column (starts at 0): ");
            fflush(stdin);
        }
        while (scanf("%d%d", &row, &column) != 2 || playMove(board, row, column, HUMAN) == false);
        printf("\n");
        if (isUndecided(board))
        {
            print(board);
            printf("\n");
            doComputerMove(board);
            printf("\n");
        }
    }
    while (isUndecided(board));
    print(board);
    if (isSideTheWinner(board, COMPUTER))
    {
        printf("Computer wins!!\n");
    }
    else
    {
        if (isSideTheWinner(board, HUMAN))
        {
            printf("Human wins!!\n");
        }
        else
        {
            printf("Draw!!\n");
        }
    }
}

void lose(Board board)
{
    // place computer in losing situation
    playMove(board, 0, 0, HUMAN);
    playMove(board, 0, 1, COMPUTER);
    playMove(board, 1, 1, HUMAN);
    print(board);
    doComputerMove(board);
#if SOLUTION != 0
    printf("\n");
    play(board, false);
#endif
}

#ifdef ANALYSE
#ifdef WIN32
long long getExecutionTime(const StopWatchData *pStopWatchData);
#else
clock_t getExecutionTime(const StopWatchData *pStopWatchData);
#endif
void printStopWatchError();

void initStopWatch(StopWatchData *pStopWatchData)
{
    pStopWatchData->running = false;
    pStopWatchData->start_time = 0;
#ifdef WIN32
    LARGE_INTEGER performanceFrequency;
    if (QueryPerformanceFrequency(&performanceFrequency) == 0)
    {
        printStopWatchError();
    }
    pStopWatchData->frequency = (double)performanceFrequency.QuadPart;
#endif
}

void startStopWatch(StopWatchData *pStopWatchData)
{
    if (!pStopWatchData->running)
    {
        pStopWatchData->running = true;
        pStopWatchData->start_time = getExecutionTime(pStopWatchData);
    }
}

void stopStopWatch(StopWatchData *pStopWatchData)
{
    if (pStopWatchData->running)
    {
        pStopWatchData->running = false;
        pStopWatchData->total_time = getExecutionTime(pStopWatchData) - pStopWatchData->start_time;
    }
}

#ifdef WIN32
long long getExecutionTime(const StopWatchData *pStopWatchData)
{
    LARGE_INTEGER performanceCount;
    if (QueryPerformanceCounter(&performanceCount) == 0)
    {
        printStopWatchError();
        return 0;
    }
    return performanceCount.QuadPart;
}
#else
clock_t getExecutionTime(const StopWatchData *pStopWatchData)
{
    clock_t t = clock();
    if (t == -1)
    {
        printStopWatchError();
        return 0;
    }
    return clock();
}
#endif

void printStopWatchError()
{
#ifdef WIN32
    LPTSTR lpMsgBuf = 0;
    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        GetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        lpMsgBuf,
        0,
        NULL
    );
    fprintf(stderr, "%s", lpMsgBuf);
    LocalFree(lpMsgBuf);
#else
    fprintf(stderr, "No timer available\n");
#endif
}

double readStopWatchTime(const StopWatchData *pStopWatchData)
{
#ifdef WIN32
    long long res = pStopWatchData->running ? getExecutionTime(pStopWatchData) - pStopWatchData->start_time : pStopWatchData->total_time;
    return res / pStopWatchData->frequency;
#else
    clock_t res = pStopWatchData->running ? getExecutionTime(pStopWatchData) - pStopWatchData->start_time : pStopWatchData->total_time;
    return (double)res / CLK_TCK;
#endif
}

void printStopWatchTime(const StopWatchData *pStopWatchData)
{
    printf("%f sec", readStopWatchTime(pStopWatchData));
}
#endif

int main(void)
{
    printf("Welcome to TIC-TAC-TOE test for losing\n");
    Board board;
    makeEmpty(board);
    lose(board);
    return 0;
}
